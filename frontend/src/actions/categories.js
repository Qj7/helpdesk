import {
  GET_CATEGORIES_REQUEST,
  GET_CATEGORIES_SUCCESS,
  POST_CATEGORY_REQUEST,
  PUT_CATEGORY_REQUEST,
  DELETE_CATEGORY_REQUEST,
} from "./types";

export const getCategoriesRequest = () => ({
  type: GET_CATEGORIES_REQUEST,
});

export const getCategoriesSuccess = (data) => ({
  type: GET_CATEGORIES_SUCCESS,
  payload: data,
});

export const postCategoryRequest = ([data]) => ({
  type: POST_CATEGORY_REQUEST,
  payload: data,
});

export const putCategoryRequest = (data) => ({
  type: PUT_CATEGORY_REQUEST,
  payload: data,
});

export const deleteCategoryRequest = ([data]) => ({
  type: DELETE_CATEGORY_REQUEST,
  payload: data,
});
