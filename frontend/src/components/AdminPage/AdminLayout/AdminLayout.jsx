import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import menu from "./menu";
import SideBar from "../../SideBar/SideBar";
import Footer from "../../Footer/Footer";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    width: "100%",
    maxWidth: theme.breakpoints.values.xl,
    flexGrow: 1,
    padding: theme.spacing(3, 3, 7, 3),
    [theme.breakpoints.up(1300)]: {
      margin: theme.spacing("auto", 10),
    },
  },
}));

const AdminLayout = () => {
  const classes = useStyles();

  return (
    <Router>
      <div className={classes.root}>
        <SideBar menu={menu} />
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Switch>
            {menu.map((route) => (
              <Route key={route.id} path={route.path} exact={route.exact}>
                {route.component}
              </Route>
            ))}
          </Switch>
        </main>
        <Footer />
      </div>
    </Router>
  );
};
export default AdminLayout;
