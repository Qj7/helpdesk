import axios from "axios";

export function deleteCategory(data) {
  const { id } = data;
  return axios.delete(`http://localhost:3000/api/v1/category/${id}`, {});
}
