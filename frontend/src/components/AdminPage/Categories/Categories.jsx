import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import CancelIcon from "@material-ui/icons/Cancel";
import {
  EditingState,
  PagingState,
  IntegratedPaging,
} from "@devexpress/dx-react-grid";
import {
  Grid,
  Table,
  TableHeaderRow,
  TableEditRow,
  TableEditColumn,
  PagingPanel,
} from "@devexpress/dx-react-grid-material-ui";
import { useDispatch, useSelector } from "react-redux";
import {
  getCategoriesRequest,
  postCategoryRequest,
  putCategoryRequest,
  deleteCategoryRequest,
} from "actions";
import { tableMessages, pagingPanelMessages } from "utils";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
}));

const getRowId = (row) => row.id;

const Categories = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.categories.categories);
  const rows = data.reverse();
  useEffect(() => {
    dispatch(getCategoriesRequest());
  }, [dispatch]);

  const [columns] = useState([
    { name: "title", title: "Название" },
    { name: "time", title: "Допустимое время" },
    { name: "points", title: "Баллы", width: 300 },
  ]);
  const [tableColumnExtensions] = useState([
    { columnName: "time", width: 180 },
    { columnName: "points", width: 180 },
  ]);
  const [pageSizes] = useState([5, 10, 20, 0]);
  const classes = useStyles();

  const AddButton = ({ onExecute }) => (
    <div style={{ textAlign: "left" }}>
      <Button
        onClick={onExecute}
        variant="contained"
        color="primary"
        className={classes.button}
      >
        Добавить
      </Button>
    </div>
  );

  const EditButton = ({ onExecute }) => (
    <Button
      onClick={onExecute}
      variant="contained"
      color="primary"
      size="small"
      className={classes.button}
      startIcon={<EditIcon />}
    >
      Редактировать
    </Button>
  );

  const DeleteButton = ({ onExecute }) => (
    <Button
      onClick={() => {
        // eslint-disable-next-line
      if (window.confirm('Вы действительно хотите удалить эту категорию')) {
          onExecute();
        }
      }}
      variant="contained"
      color="secondary"
      size="small"
      className={classes.button}
      startIcon={<DeleteIcon />}
    >
      Удалить
    </Button>
  );

  const CommitButton = ({ onExecute }) => (
    <Button
      onClick={onExecute}
      variant="contained"
      color="primary"
      size="small"
      className={classes.button}
      startIcon={<SaveIcon />}
    >
      Сохранить
    </Button>
  );

  const CancelButton = ({ onExecute }) => (
    <Button
      onClick={onExecute}
      variant="contained"
      color="default"
      size="small"
      className={classes.button}
      startIcon={<CancelIcon />}
    >
      Отмена
    </Button>
  );

  const commandComponents = {
    add: AddButton,
    edit: EditButton,
    delete: DeleteButton,
    commit: CommitButton,
    cancel: CancelButton,
  };

  const Command = ({ id, onExecute }) => {
    const CommandButton = commandComponents[id];
    return <CommandButton onExecute={onExecute} />;
  };

  const commitChanges = ({ added, changed, deleted }) => {
    if (added) {
      dispatch(postCategoryRequest(added));
    }
    if (changed) {
      dispatch(putCategoryRequest(changed));
    }
    if (deleted) {
      dispatch(deleteCategoryRequest(deleted));
    }
  };

  return (
    <Card>
      <CardHeader
        title="Категории заявок"
        subheader="Добавить/удалить категорию и
        установить баллы"
      />
      <CardContent>
        <Grid rows={rows} columns={columns} getRowId={getRowId}>
          <EditingState onCommitChanges={commitChanges} />
          <PagingState defaultCurrentPage={0} defaultPageSize={5} />
          <IntegratedPaging />
          <Table
            columnExtensions={tableColumnExtensions}
            messages={tableMessages}
          />
          <TableHeaderRow />
          <TableEditRow />
          <TableEditColumn
            showAddCommand
            showEditCommand
            showDeleteCommand
            commandComponent={Command}
            width={320}
          />
          <PagingPanel pageSizes={pageSizes} messages={pagingPanelMessages} />
        </Grid>
      </CardContent>
    </Card>
  );
};

export default Categories;
