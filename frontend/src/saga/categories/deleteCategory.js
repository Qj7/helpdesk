import { call, put } from "redux-saga/effects";
import { deleteCategory as deleteCategoryApi } from "api";
import { getCategoriesRequest } from "actions";
import { errorHandler } from "utils";

export function* deleteCategory(action) {
  try {
    yield call(deleteCategoryApi, { id: action.payload });
    yield put(getCategoriesRequest());
  } catch (err) {
    errorHandler(err);
  }
}
