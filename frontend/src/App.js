/* eslint-disable react/jsx-filename-extension */
import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import SignIn from "./components/LoginPage/SignIn/SignIn";
import AdminLayout from "./components/AdminPage/AdminLayout/AdminLayout";
import UserLayout from "./components/UserPage/UserLayout/UserLayout";
import IssueCard from "./components/UserPage/IssueCard/IssueCard";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={SignIn} />
        <Route path="/admin" component={AdminLayout} />
        <Route path="/userpage" component={UserLayout} />
        <Route path="/issuecard" component={IssueCard} />
      </Switch>
    </Router>
  );
}
export default App;
