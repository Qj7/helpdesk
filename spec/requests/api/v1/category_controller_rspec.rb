# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Widget management', type: :request do

  it 'response all category' do
    get 'http://localhost:3000/api/v1/category.json'
    expect(response).to render_template(:api_v1_category_url)
  end

end
