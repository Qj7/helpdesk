import React from "react";
import { Grid } from "@material-ui/core";
import AddTask from "../AddTask/AddTask";
import Issues from "../Issues/Issues";

const Home = () => {
  return (
    <Grid container spacing={3}>
      <Grid item sm={12} md={6}>
        <AddTask />
      </Grid>
      <Grid item sm={12} md={6}>
        <Issues />
      </Grid>
    </Grid>
  );
};

export default Home;
