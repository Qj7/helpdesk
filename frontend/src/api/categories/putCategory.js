import axios from "axios";

export function putCategory({ data }) {
  const id = Object.keys(data)[0];
  const { title, time, points } = data[id];
  return axios.put(`http://localhost:3000/api/v1/category/${id}`, {
    title,
    time,
    points,
  });
}
