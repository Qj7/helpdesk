import { call, put } from "redux-saga/effects";
import { putCategory as putCategoryApi } from "api";
import { getCategoriesRequest } from "actions";
import { errorHandler } from "utils";

export function* putCategory(action) {
  try {
    yield call(putCategoryApi, { data: action.payload });
    yield put(getCategoriesRequest());
  } catch (err) {
    errorHandler(err);
  }
}
