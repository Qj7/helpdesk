# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

category = Category.create(
  [
    {
      title: "Интернет",
    },
    {
      title: "1С УТП"
    },
    {
      title: "1С ЗУП"
    },
    {
      title: "1С УАТ"
    },
    {
      title: "1С Прочие"
    },
    {
      title: "Прочие программы"
    },
    {
      title: "Оборудование, оргтехника"
    },
    {
      title: "Доступы, пароли"
    },
    {
      title: "Звонки Астериск"
    },
    {
      title: "Телефония"
    },
    {
      title: "Коммуникация"
    },
    {
      title: "АРМ ОС Коммунальные платежи"
    },
    {
      title: "АРМ ОС Почтовые переводы"
    },
    {
      title: "АРМ ОС Электронные платежи"
    },
    {
      title: "АРМ ОС Технические вопросы"
    }
  ])
