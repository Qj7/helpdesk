import React from "react";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";

const MyTasks = () => {
  return (
    <Card>
      <CardHeader title="Мои заявки" />
    </Card>
  );
};

export default MyTasks;
