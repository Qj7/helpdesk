import { call, put } from "redux-saga/effects";
import { getCategories as getCategoriesApi } from "api";
import { getCategoriesSuccess } from "actions";
import { errorHandler } from "utils";

export function* getCategories() {
  try {
    const data = yield call(getCategoriesApi);
    yield put(getCategoriesSuccess({ data: data.data }));
  } catch (err) {
    errorHandler(err);
  }
}
