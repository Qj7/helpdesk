/* eslint-disable react/jsx-filename-extension */
import React from "react";
import SpeedIcon from "@material-ui/icons/Speed";
import AssignmentOutlinedIcon from "@material-ui/icons/AssignmentOutlined";
import AssignmentIcon from "@material-ui/icons/Assignment";
import CommentIcon from "@material-ui/icons/Comment";
import PeopleIcon from "@material-ui/icons/People";
import VisibilityIcon from "@material-ui/icons/Visibility";
import TimerIcon from "@material-ui/icons/Timer";

import Dashboard from "../Dashboard/Dashboard";
import AllTasks from "../AllTasks/AllTasks";
import MyTasks from "../MyTasks/MyTasks";
import Comments from "../Comments/Comments";
import Users from "../Users/Users";
import Categories from "../Categories/Categories";
import ResponseTime from "../ResponseTime/ResponseTime";

const menu = [
  {
    id: "Dashboard",
    icon: <SpeedIcon />,
    label: "Dashboard",
    path: "/dashboard",
    component: <Dashboard />,
  },
  {
    id: "All tasks",
    icon: <AssignmentOutlinedIcon />,
    label: "Все заявки",
    path: "/all-tasks",
    component: <AllTasks />,
  },
  {
    id: "My tasks",
    icon: <AssignmentIcon />,
    label: "Мои заявки",
    path: "/my-tasks",
    component: <MyTasks />,
  },
  {
    id: "Comments",
    icon: <CommentIcon />,
    label: "Комментарии",
    path: "/comments",
    component: <Comments />,
  },
  {
    id: "Users",
    icon: <PeopleIcon />,
    label: "Пользователи",
    path: "/users",
    component: <Users />,
  },
  {
    id: "Categories",
    icon: <VisibilityIcon />,
    label: "Категории",
    path: "/categories",
    component: <Categories />,
  },
  {
    id: "Response time",
    icon: <TimerIcon />,
    label: "Время реагирования",
    path: "/response-time",
    component: <ResponseTime />,
  },
];

export default menu;
