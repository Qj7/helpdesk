import { call, put } from "redux-saga/effects";
import { postCategory as postCategoryApi } from "api";
import { getCategoriesRequest } from "actions";
import { errorHandler } from "utils";

export function* postCategory(action) {
  try {
    yield call(postCategoryApi, { data: action.payload });
    yield put(getCategoriesRequest());
  } catch (err) {
    errorHandler(err);
  }
}
