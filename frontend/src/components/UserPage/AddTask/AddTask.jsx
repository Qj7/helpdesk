import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useFormik } from "formik";
import * as Yup from "yup";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Snackbar from "@material-ui/core/Snackbar";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { getCategoriesRequest, postTaskRequest } from "actions";

const useStyle = makeStyles((theme) => ({
  dropList: {
    width: "100%",
  },
  textField: {
    width: "100%",
    margin: theme.spacing(2, 0),
  },
}));

const AddTask = () => {
  const dispatch = useDispatch();
  const subject = useSelector((state) => state.categories.categories);

  useEffect(() => {
    dispatch(getCategoriesRequest());
  }, [dispatch]);

  const classes = useStyle();

  const [openSnackbar, setOpenSnackbar] = useState(false);
  const handleCloseSnackbar = () => {
    setOpenSnackbar(false);
  };
  const formik = useFormik({
    initialValues: { subject: "", topic: "", body: "" },
    validationSchema: Yup.object({
      subject: Yup.string().ensure().required("Required"),
      topic: Yup.string()
        .max(50, "Must be 50 characters or less")
        .required("Required"),
      body: Yup.string()
        .max(500, "Must be 500 characters or less")
        .required("Required"),
    }),
    onSubmit: (values) => {
      dispatch(postTaskRequest(values));
      formik.resetForm();
      setOpenSnackbar(true);
    },
  });
  return (
    <Card>
      <CardHeader
        title="Создать задачу"
        subheader="Выберите, что у Вас случилось и опишите проблему"
      />
      <CardContent>
        <form onSubmit={formik.handleSubmit}>
          <FormControl className={classes.dropList}>
            <InputLabel id="subject-select-label">Категории</InputLabel>
            <Select
              labelId="subject-select-label"
              id="subject"
              name="subject"
              value={formik.values.subject}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.errors.subject && formik.touched.subject}
            >
              {subject.map((i) => (
                <MenuItem key={i.id} value={i.title}>
                  {i.title}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          <TextField
            id="topic"
            label="Тема"
            name="topic"
            value={formik.values.topic}
            className={classes.textField}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.errors.topic && formik.touched.topic}
            helperText={
              formik.errors.topic && formik.touched.topic && formik.errors.topic
            }
          />

          <TextField
            id="body"
            label="Описание"
            name="body"
            value={formik.values.body}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.errors.body && formik.touched.body}
            helperText={
              formik.errors.body &&
              formik.touched.body &&
              formik.errors.body
            }
            multiline
            rows={4}
            placeholder="Опишите проблему"
            variant="outlined"
            className={classes.textField}
          />

          <Button type="submit" variant="contained" color="primary">
            Создать
          </Button>
        </form>
      </CardContent>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
        message="Ваша заявка отправлена"
      />
    </Card>
  );
};

export default AddTask;
