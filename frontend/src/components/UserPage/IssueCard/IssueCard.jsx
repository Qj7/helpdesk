import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import InsertDriveFileOutlinedIcon from "@material-ui/icons/InsertDriveFileOutlined";
import FiberManualRecordOutlinedIcon from "@material-ui/icons/FiberManualRecordOutlined";
import PersonOutlineOutlinedIcon from "@material-ui/icons/PersonOutlineOutlined";
import CalendarTodayOutlinedIcon from "@material-ui/icons/CalendarTodayOutlined";
import ScheduleOutlinedIcon from "@material-ui/icons/ScheduleOutlined";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  table: {
    border: "none",
  },
  actions: {
    display: "block",
    marginRight: theme.spacing(1),
  },
  textarea: {
    width: "100%",
    minHeight: "calc(25% - 50px)",
    display: "flex",
    flexDirection: "column",
  },
  button: {
    margin: theme.spacing(1),
  },
}));
function createData(name, value) {
  return { name, value };
}

const rows1 = [
  createData("Категория", "Интернет"),
  createData("Тема", "test"),
  createData("Описание", "test2"),
];

const rows2 = [
  createData("Статус", "В работе"),
  createData("Открыта", "30 апр. 2020 16:14"),
];

const IssueCard = () => {
  const classes = useStyles();

  return (
    <Card>
      <CardHeader
        title={`Заявка №${5}`}
        subheader="Все обновления заявки тут"
      />

      <CardContent>
        <Typography variant="h5" component="h5">
          <InsertDriveFileOutlinedIcon />
          Test
        </Typography>
        <Grid container spacing={3}>
          <Grid item>
            <Typography variant="body1">
              Приоритет:
              <FiberManualRecordOutlinedIcon fontSize="small" />
              Обычный
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body1">
              Открыта:
              <PersonOutlineOutlinedIcon fontSize="small" />
              user2
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body1">
              <CalendarTodayOutlinedIcon fontSize="small" />
              26 апр. 2020
              <ScheduleOutlinedIcon fontSize="small" />
              10:52
            </Typography>
          </Grid>
        </Grid>
        <Divider />
        <Typography variant="h6">Описание:</Typography>
        <TableContainer>
          <Grid
            container
            justify="space-between"
            component={Table}
            size="small"
            className={classes.root}
          >
            <Grid item component={TableBody}>
              {rows1.map((row) => (
                <TableRow key={row.name}>
                  <TableCell
                    component="th"
                    scope="row"
                    className={classes.table}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell
                    style={{ minWidth: 400 }}
                    align="right"
                    className={classes.table}
                  >
                    {row.value}
                  </TableCell>
                </TableRow>
              ))}
            </Grid>
            <Grid item component={TableBody}>
              {rows2.map((row) => (
                <TableRow key={row.name}>
                  <TableCell
                    component="th"
                    scope="row"
                    className={classes.table}
                  >
                    {row.name}
                  </TableCell>
                  <TableCell
                    style={{ width: 400 }}
                    align="right"
                    className={classes.table}
                  >
                    {row.value}
                  </TableCell>
                </TableRow>
              ))}
            </Grid>
          </Grid>
        </TableContainer>
      </CardContent>
      <CardActions className={classes.actions}>
        <Typography variant="h6">Комментарии:</Typography>
        <TextareaAutosize
          aria-label="empty textarea"
          className={classes.textarea}
        />
        <Button variant="contained" color="primary" className={classes.button}>
          Написать
        </Button>
      </CardActions>
    </Card>
  );
};

export default IssueCard;
