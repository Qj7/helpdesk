import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import {
  SearchState,
  IntegratedFiltering,
  SortingState,
  IntegratedSorting,
  PagingState,
  IntegratedPaging,
} from "@devexpress/dx-react-grid";
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  SearchPanel,
  PagingPanel,
} from "@devexpress/dx-react-grid-material-ui";
import { tableMessages, SearchPanelMessages, pagingPanelMessages } from "utils"

function generateRows(id, user, topic, category, description, created) {
  return { id, user, topic, category, description, created };
}

const data = [
  generateRows("1", "user", "Подключение", "Интернет", "", "20.05"),
  generateRows("2", "user", "Подключение", "Интернет", "", "22.05"),
  generateRows("3", "user", "Подключение", "Интернет", "", "02.05"),
  generateRows("4", "user", "Подключение", "Интернет", "", "14.05"),
  generateRows("5", "user", "Подключение", "Интернет", "", "25.05"),
  generateRows("6", "user", "Подключение", "Интернет", "", "11.05"),
];

const AllTasks = () => {
  const [columns] = useState([
    { name: "id", title: "#" },
    { name: "user", title: "Создал" },
    { name: "topic", title: "Тема" },
    { name: "category", title: "Категория" },
    { name: "description", title: "Описание" },
    { name: "created", title: "Создана" },
  ]);
  const [rows] = useState(data);
  const [searchValue, setSearchState] = useState("");
  const [currentPage, setCurrentPage] = useState(0);
  const [pageSize, setPageSize] = useState(5);
  const [pageSizes] = useState([5, 10, 15]);

  return (
    <Card>
      <CardHeader title="Все заявки" subheader="Subheader.." />
      <CardContent>
        <Grid rows={rows} columns={columns}>
          <SearchState value={searchValue} onValueChange={setSearchState} />
          <SortingState
            defaultSorting={[{ columnName: "id", direction: "asc" }]}
          />
          <PagingState
            currentPage={currentPage}
            onCurrentPageChange={setCurrentPage}
            pageSize={pageSize}
            onPageSizeChange={setPageSize}
          />
          <IntegratedFiltering />
          <IntegratedSorting />
          <IntegratedPaging />
          <Table messages={tableMessages} />
          <TableHeaderRow showSortingControls />
          <Toolbar />
          <SearchPanel messages={SearchPanelMessages} />
          <PagingPanel pageSizes={pageSizes} messages={pagingPanelMessages} />
        </Grid>
      </CardContent>
    </Card>
  );
};

export default AllTasks;
