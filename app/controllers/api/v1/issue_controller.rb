# frozen_string_literal: true

module Api::V1
  class IssueController < ApplicationController
   def index
      issue = Issue.all
      render json: issue
    end
    def create
      issue = Issue.new(issue_params)
      if issue.save
      render json: issue 
      else
        head 422
      end
    end
    def issue_params 
      params.permit(:subject, :body)
    end
end
end
