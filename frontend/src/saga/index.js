import { takeEvery, takeLatest, all } from "redux-saga/effects";

import {
  GET_CATEGORIES_REQUEST,
  POST_CATEGORY_REQUEST,
  PUT_CATEGORY_REQUEST,
  DELETE_CATEGORY_REQUEST,
  GET_TASKS_REQUEST,
  POST_TASK_REQUEST,
} from "actions";

import {
  getCategories,
  postCategory,
  putCategory,
  deleteCategory,
} from "./categories";

import { getTasks, postTask } from "./tasks";

export function* rootSaga() {
  yield all([
    yield takeLatest(GET_CATEGORIES_REQUEST, getCategories),
    yield takeEvery(POST_CATEGORY_REQUEST, postCategory),
    yield takeEvery(PUT_CATEGORY_REQUEST, putCategory),
    yield takeEvery(DELETE_CATEGORY_REQUEST, deleteCategory),
    yield takeLatest(GET_TASKS_REQUEST, getTasks),
    yield takeEvery(POST_TASK_REQUEST, postTask),
  ]);
}
