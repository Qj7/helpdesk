export const tableMessages = {
    noData: "В таблице отсутствуют данные",
};
  
export const SearchPanelMessages = {
    searchPlaceholder: "Поиск...",
};
  
export const pagingPanelMessages = {
    showAll: "Все",
    rowsPerPage: "Показать записей",
    info: "{from} - {to} (с {count} записей)",
};