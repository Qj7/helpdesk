import { GET_CATEGORIES_SUCCESS } from "actions";

const initialState = {
  categories: [],
};

export const categoriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CATEGORIES_SUCCESS:
      return { ...state, categories: action.payload.data };
    default:
      return state;
  }
};
