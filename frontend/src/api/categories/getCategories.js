import axios from "axios";

export function getCategories() {
  return axios.get("http://localhost:3000/api/v1/category/");
}
