import { GET_TASKS_SUCCESS } from "actions";

const initialState = {
  tasks: [],
};

export const tasksReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TASKS_SUCCESS:
      return { ...state, tasks: action.payload.data };
    default:
      return state;
  }
};
