Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :category
    end
    namespace :v1 do
      resources :issue
    end
  end
end
