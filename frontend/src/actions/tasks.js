import {
  GET_TASKS_REQUEST,
  GET_TASKS_SUCCESS,
  POST_TASK_REQUEST,
} from "./types";

export const getTasksRequest = () => ({
  type: GET_TASKS_REQUEST,
});

export const getTasksSuccess = (data) => ({
  type: GET_TASKS_SUCCESS,
  payload: data,
});

export const postTaskRequest = (data) => ({
  type: POST_TASK_REQUEST,
  payload: data,
});
