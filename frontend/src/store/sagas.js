import axios from "axios";
import { call, put, takeEvery, takeLatest, all } from "redux-saga/effects";
import {
  GET_CATEGORIES_REQUEST,
  POST_CATEGORY_REQUEST,
  PUT_CATEGORY_REQUEST,
  DELETE_CATEGORY_REQUEST,
  GET_TASKS_REQUEST,
  POST_TASK_REQUEST,
} from "./types";
import {
  getCategoriesSuccess,
  postCategorySuccess,
  getTasksSuccess,
} from "./actions";

// requests
function getCategories() {
  return axios.get("http://localhost:3000/api/v1/category/");
}

function postCategory({ data }) {
  return axios.post("http://localhost:3000/api/v1/category/", {
    title: data.title
  });
}
export function putCategory({ data }) {
  const id = Object.keys(data)[0];
  const { title, time, points } = data[id];
  return axios.put(`http://localhost:3000/api/v1/category/${id}`, {
    title,
    time,
    points
  });
}

function deleteCategory({ id }) {
  return axios.delete(`http://localhost:3000/api/v1/category/${id}`, {});
}
function getTasks() {
  return axios.get("https://jsonplaceholder.typicode.com/todos");
}

function postNewTask({ data }) {
  console.log(data);
  return axios.post("http://localhost:3000/api/v1/issue", {
    id: 1,
    title: data.topic
  });
}

// workers
function* getCategoriesRequest() {
  try {
    const data = yield call(getCategories);
    yield put(getCategoriesSuccess({ data: data.data }));
  } catch (e) {
    console.log(e);
  }
}

function* postCategoryRequest(action) {
  try {
    yield call(postCategory, { data: action.payload });
    yield put({
      type: GET_CATEGORIES_REQUEST,
    });  } catch (e) {
    console.log(e);
  }
}

function* putCategoryRequest(action) {
  try {
    yield call(putCategory, { data: action.payload });
    yield put({
      type: GET_CATEGORIES_REQUEST,
    });
  } catch (e) {
    console.log(e);
  }
}

function* deleteCategoryRequest(action) {
  try {
    yield call(deleteCategory, { id: action.payload });
    yield put({
      type: GET_CATEGORIES_REQUEST,
    });
  } catch (e) {
    yield console.log(e);
  }
}

function* getTasksRequest() {
  try {
    const data = yield call(getTasks);
    yield put(getTasksSuccess({ data: data.data }));
  } catch (e) {
    console.log(e);
  }
}

function* postTaskRequest(action) {
  try {
    yield call(postNewTask, { data: action.payload });
    yield put({
      type: GET_TASKS_REQUEST,
    });
  } catch (e) {
    console.log(e);
  }
}

export default function* rootSaga() {
  yield all([
    yield takeLatest(GET_CATEGORIES_REQUEST, getCategoriesRequest),
    yield takeEvery(POST_CATEGORY_REQUEST, postCategoryRequest),
    yield takeEvery(PUT_CATEGORY_REQUEST, putCategoryRequest),
    yield takeEvery(DELETE_CATEGORY_REQUEST, deleteCategoryRequest),
    yield takeLatest(GET_TASKS_REQUEST, getTasksRequest),
    yield takeEvery(POST_TASK_REQUEST, postTaskRequest),
  ]);
}
