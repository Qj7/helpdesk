import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  footer: {
    position: "fixed",
    left: "0px",
    bottom: "0px",
    width: "100%",
    backgroundColor: "#007BFF",
    padding: "6px 0",
    textAlign: "center",
    color: "#fff"
  }
});

const Footer = props => {
  const classes = useStyles();
  return (
    <div className={classes.footer}>
      <small> footer Post ©2020 </small>
    </div>
  );
};
export default Footer;
