import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom"; // redirect
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import CardHeader from "@material-ui/core/CardHeader";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    width: 400,
    margin: `${theme.spacing(0)} auto`,
  },
  loginBtn: {
    marginTop: theme.spacing(2),
    flexGrow: 1,
  },
  header: {
    textAlign: "center",
    background: "#007BFF",
    color: "#fff",
  },
  card: {
    marginTop: theme.spacing(10),
  },
}));

const SignIn = () => {
  const history = useHistory(); // for redirect
  const classes = useStyles();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isButtonActive, setIsButtonActive] = useState(false);
  const [helperText, setHelperText] = useState("");
  const [error, setError] = useState(false);

  const routeChange = (path) => {
    setError(false);
    setHelperText("SignIn Successfully");
    history.push(path);
  };

  useEffect(() => {
    if (username.trim() && password.trim()) {
      setIsButtonActive(true);
    } else {
      setIsButtonActive(false);
    }
  }, [username, password]);

  const handleSignIn = () => {
    if (username === "admin" && password === "admin") {
      routeChange("admin"); // SignIn Successfully
    } else if (username === "user" && password === "user") {
      routeChange("userpage"); // SignIn Successfully
    } else if (username === "userp" && password === "user") {
      routeChange("issuecard"); // SignIn Successfully
    } else {
      setError(true);
      setHelperText("Incorrect username or password");
    }
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter" && isButtonActive) {
      handleSignIn();
    }
  };

  return (
    <>
      <form className={classes.container} noValidate autoComplete="off">
        <Card className={classes.card}>
          <CardHeader className={classes.header} title="HelpDesk" />
          <CardContent>
            <div>
              <TextField
                error={error}
                fullWidth
                id="username"
                type="email"
                label="Username"
                placeholder="Username"
                margin="normal"
                onChange={(e) => setUsername(e.target.value)}
                onKeyDown={(e) => handleKeyPress(e)}
              />
              <TextField
                error={error}
                fullWidth
                id="password"
                type="password"
                label="Password"
                placeholder="Password"
                margin="normal"
                helperText={helperText}
                onChange={(e) => setPassword(e.target.value)}
                onKeyPress={(e) => handleKeyPress(e)}
              />
            </div>
          </CardContent>
          <CardActions>
            <Button
              variant="contained"
              size="large"
              color="secondary"
              className={classes.loginBtn}
              onClick={() => handleSignIn()}
              disabled={!isButtonActive}
            >
              Войти
            </Button>
          </CardActions>
        </Card>
      </form>
    </>
  );
};

export default SignIn;
