import { call, put } from "redux-saga/effects";
import { getTasks as getTasksApi } from "api";
import { getTasksSuccess } from "actions";
import { errorHandler } from "utils";

export function* getTasks() {
  try {
    const data = yield call(getTasksApi);
    yield put(getTasksSuccess({ data: data.data }));
  } catch (err) {
    errorHandler(err);
  }
}
