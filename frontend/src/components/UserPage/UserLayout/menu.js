/* eslint-disable react/jsx-filename-extension */
import React from "react";
import HomeIcon from "@material-ui/icons/Home";
import Home from "../Home/Home";

const menu = [
  {
    id: "Home",
    icon: <HomeIcon />,
    label: "Домой",
    path: "/userpage",
    component: <Home />,
  },
];

export default menu;
