# frozen_string_literal: true

module Api::V1
  class CategoryController < ApplicationController
    def index
      category = Category.all
      render json: category
    end

    def destroy
      category = Category.find(params[:id])
      category.destroy
      head 204
    end

    def create
      category = Category.new(category_params)
      if category.save
      render json: category
      else
        head 422
      end
    end

    def update
      category = Category.find(params[:id])
      if category.update(category_params)
        render json: category
      else
        head 422
      end
    end

    private

    def category_params
      params.require(:category).permit(:id, :title)
    end
  end
end
