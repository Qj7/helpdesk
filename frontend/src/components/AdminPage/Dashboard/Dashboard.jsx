import React from "react";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";

const Dashboard = () => {
  return (
    <Card>
      <CardHeader title="Dashboard" />
    </Card>
  );
};

export default Dashboard;
