class CreateIssues < ActiveRecord::Migration[5.2]
  def change
    create_table :issues do |t|
      t.string :subject
      t.string :status
      t.string :body
      t.string :classification
      t.string :priority
      t.datetime :open_at
      t.references :author
      t.references :owner
      t.timestamps null: false
    end
  end

end
