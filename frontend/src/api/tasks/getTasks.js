import axios from "axios";

export function getTasks() {
  return axios.get("https://jsonplaceholder.typicode.com/todos");
}
