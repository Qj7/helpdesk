import axios from "axios";

export function postTask({ data }) {
  const { subject, topic, body } = data;

  return axios.post("http://localhost:3000/api/v1/issue", {
    subject,
    topic,
    body,
  });
}
