import { call, put } from "redux-saga/effects";
import { postTask as postTasApi } from "api";
import { getTasksRequest } from "actions";
import { errorHandler } from "utils";

export function* postTask(action) {
  try {
    yield call(postTasApi, { data: action.payload });
    yield put(getTasksRequest());
  } catch (err) {
    errorHandler(err);
  }
}
