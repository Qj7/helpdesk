import axios from "axios";

export function postCategory({ data }) {
  const { title, time, points } = data;

  return axios.post("http://localhost:3000/api/v1/category/", {
    title,
    time,
    points,
  });
}
