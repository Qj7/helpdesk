import React, { useState, useEffect } from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { Template } from "@devexpress/dx-react-core";
import {
  SearchState,
  IntegratedFiltering,
  SortingState,
  IntegratedSorting,
  PagingState,
  IntegratedPaging,
} from "@devexpress/dx-react-grid";
import {
  Grid,
  Table,
  TableHeaderRow,
  Toolbar,
  SearchPanel,
  PagingPanel,
} from "@devexpress/dx-react-grid-material-ui";
import { useDispatch, useSelector } from "react-redux";
import { getTasksRequest } from "actions";
import { tableMessages, SearchPanelMessages, pagingPanelMessages } from "utils";

const useStyle = makeStyles(() => ({
  header: {
    marginRight: "auto",
  },
}));

const Issues = () => {
  const classes = useStyle();
  const dispatch = useDispatch();
  const data = useSelector((state) => state.tasks.tasks);
  const rows = data.reverse();

  useEffect(() => {
    dispatch(getTasksRequest());
  }, [dispatch]);

  const [columns] = useState([
    { name: "id", title: "#" },
    { name: "topic", title: "Тема" },
    { name: "status", title: "Статус" },
    { name: "comment", title: "Комментарий" },
  ]);
  const [tableColumnExtensions] = useState([
    { columnName: "id", width: 80 },

    { columnName: "status", width: 150 },
    { columnName: "comment", width: 150 },
  ]);
  const [searchValue, setSearchState] = useState("");
  const [pageSizes] = useState([5, 10, 20, 0]);

  return (
    <Card>
      <CardContent>
        <Grid rows={rows} columns={columns}>
          <SearchState value={searchValue} onValueChange={setSearchState} />
          <SortingState
            defaultSorting={[{ columnName: "id", direction: "asc" }]}
          />
          <PagingState defaultCurrentPage={0} defaultPageSize={5} />
          <IntegratedFiltering />
          <IntegratedSorting />
          <IntegratedPaging />
          <Table
            columnExtensions={tableColumnExtensions}
            messages={tableMessages}
          />
          <TableHeaderRow showSortingControls />
          <Toolbar />
          <Template name="toolbarContent">
            <Typography variant="h5" className={classes.header}>
              Мои задачи
            </Typography>
          </Template>
          <SearchPanel messages={SearchPanelMessages} />
          <PagingPanel pageSizes={pageSizes} messages={pagingPanelMessages} />
        </Grid>
      </CardContent>
    </Card>
  );
};
export default Issues;
