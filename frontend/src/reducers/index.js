import { combineReducers } from "redux";
import { categoriesReducer } from "./categoriesReducer";
import { tasksReducer } from "./tasksReducer";

export const rootReducer = combineReducers({
  categories: categoriesReducer,
  tasks: tasksReducer,
});
